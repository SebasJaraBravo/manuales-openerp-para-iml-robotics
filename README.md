#Manuales OpenERP para IML Robotics, S.L.

##Introducción
Aquí ubicaremos los manuales de uso de OpenERP para el proyecto de IML Robotics, S.L., con un doble propósito:

-	Permitir el acceso al cliente a estos manuales, donde estarán siempre actualizados.

-	Llevar un control "verbal" de modificaciones del mismo entre las diferentes actualizaciones.


Para ver las últimas actualizaciones realizadas sobre los manuales, nos situaremos en el menú *Commits*
donde veremos de la más reciente a la más antigua, cada una de las actualizaciones que se han realizado
y donde en la explicación se comentarán brevemente las modificaciones realizadas. No es posible mostrar
las diferencias exactas entre las diferentes versiones de documentos, pues se trata de ficheros binarios
y este repositorio no lo permite.
 
